package com.mycompany.l02;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Homework {

    WebDriver driver;
    private final int WAIT_TIME = 10000;
    private final String ITEM_NUMBER = "32525737";

    @BeforeTest
    public void initData() {
        driver = new ChromeDriver();
        driver.get("https://www.ozon.ru/");
    }

    @AfterTest
    public void closeBrowser() {
        driver.quit();
    }

    @Test
    public void findItemByNumberAssert() throws InterruptedException {
        WebElement searchField = driver.findElement(By.name("search"));
        searchField.click();
        searchField.sendKeys(ITEM_NUMBER);
        driver.findElement(By.xpath("//button[@aria-label]")).click();

        Thread.sleep(WAIT_TIME);
        WebElement price = driver.findElement(By.xpath("//span[@class='b5v6 c8c6 c4v8']"));
        assertTrue(price.getText().contains("2 481 ₽"));
        assertEquals("rgba(249, 17, 85, 1)", price.getCssValue("color"));
    }

    @Test
    public void findItemByNumberSoftAssert() throws InterruptedException {
        WebElement searchField = driver.findElement(By.name("search"));
        searchField.click();
        searchField.sendKeys(ITEM_NUMBER);
        driver.findElement(By.xpath("//button[@aria-label]")).click();

        Thread.sleep(WAIT_TIME);
        WebElement price = driver.findElement(By.xpath("//span[@class='b5v6 c8c6 c4v8']"));

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(price.getText().contains("2 481 ₽"));
        softAssert.assertEquals("rgba(249, 17, 85, 1)", price.getCssValue("color"));
    }

}
